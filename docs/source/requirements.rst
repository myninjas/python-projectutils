************
Requirements
************

.. code-block:: ini

    [package-name]
    command = The command line, if given, overrides all other options for installing the package.
    comment = A comment regarding the package. How/where it is used, for example.
    docs = The URL for package documentation.
    env = The environment into which the package is installed: base, control, development, live, staging, testing. Defaults to base.
    extra = The extra option (included in brackets) for the package install.
    group = The package group: assets, requirements, symlinks. Defaults to requirements.
    home = The URL for the package home page.
    scm = The URL for the package repo.
    url = The URL used to install the package.
    version = The version specifier for the package. This is added as is to package name.
