# Imports

from tabulate import tabulate

# Exports

__all__ = (
    "get_choice",
    "get_input",
    "Row",
    "Table",
)

# Functions


def get_choice(label, choices=None, default=None):
    if choices is None:
        choices = ["yes", "no"]

    if default:
        _label = "%s [%s]" % (label, default)
    else:
        _label = label

    print("")
    print(_label)
    print("-" * len(_label))

    count = 1
    numbers = [count]
    for choice in choices:
        print("    %s. %s" % (count, choice))

        count += 1
        numbers.append(count)

    value = get_input("Enter Choice")

    if not value and default is not None:
        return default

    # print(type(value), value)

    if not value or int(value) not in numbers:
        print("Invalid choice ...")
        print("")

        return get_choice(label, choices=choices, default=default)

    return choices[int(value) -1]


def get_input(label, default=None):
    if default:
        _label = "%s [%s]: " % (label, default)
    else:
        _label = "%s: " % label

    print("")
    value = input(_label)

    if not value:
        return default

    return value

# Classes


class Row(object):
    """A row in tabular (command line) output."""

    def __init__(self, number=None, values=None):
        self.number = number
        self._values = values or list()

    def __iter__(self):
        return iter(self._values)


class Table(object):
    """A table for tabular (command line) output."""

    def __init__(self, headings=None, formatting="plain", name=None):
        self.format = formatting
        self.headings = headings
        self.name = name
        self._rows = list()

    def __str__(self):
        return self.to_string()

    def add(self, values):
        """Add a row to the table.

        :param values: The values of the row.
        :type values: list

        """
        row = Row(len(self._rows) + 1, values=values)
        self._rows.append(row)

    def to_string(self):
        """Get the table as string output.

        :rtype: str

        """
        # TODO: Include the table name (if given) in table output.
        return tabulate(self._rows, headers=self.headings, tablefmt=self.format)
