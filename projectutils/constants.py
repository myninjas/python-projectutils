import os

# https://opensource.org/licenses/Apache-2.0
# https://opensource.org/licenses/BSD-2-Clause
# https://opensource.org/licenses/BSD-3-Clause
# https://opensource.org/licenses/CDDL-1.0
# https://opensource.org/licenses/MIT
LICENSES = {
    'Apache': "https://opensource.org/licenses/Apache-2.0",
    'BSD3': "https://opensource.org/licenses/BSD-3-Clause",
    'MIT': "https://opensource.org/licenses/MIT",
    'Proprietary': "https://copyright.gov",
}

LICENSE_CHOICES = [
    "Apache",
    "BSD3",
    "MIT",
    "Proprietary",
]

LOGGER_NAME = "projectutils"

PROJECT_STAGES = [
    "planning",
    "experimental",
    "development",
    "alpha",
    "beta",
    "release",
    "live",
    "obsolete",
]

PROJECT_STATUSES = [
    "active",
    "hold",
    "maintenance",
    "closed",
    "unknown",
]

PROJECT_TEMPLATES = {
    'cla-corporate.markdown.j2': "CLA-Corporate.markdown",
    'cla-individual.markdown.j2': "CLA-Individual.markdown",
    'coveragerc.j2': ".coveragerc",
    'description.txt.j2': "DESCRIPTION.txt",
    'gitignore.j2': ".gitignore",
    'license.txt.j2': "LICENSE.txt",
    'Makefile.j2': "Makefile",
    'manifest.in.j2': "MANIFEST.in",
    'meta.ini.j2': "meta.ini",
    'readme.markdown.j2': "README.markdown",
    'requirements.pip.j2': "requirements.pip",
    'setup.py.j2': "setup.py",
    'sla.markdown.j2': "SLA.markdown",
    'sla.rst.j2': "SLA.rst",
    'sla.txt.j2': "SLA.txt",
    'status.j2': ".status",
}

TEMPLATE_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), "templates"))
