# Imports

from configparser import ConfigParser, NoSectionError
from datetime import datetime
import logging
from markdown import markdown
# from myninjas.shell.constants import EXIT_INPUT, EXIT_OK
from myninjas.utils import read_file, write_file
import os
from ..constants import LICENSE_CHOICES, LICENSES, LOGGER_NAME as DEFAULT_LOGGER_NAME, PROJECT_STAGES, \
    PROJECT_STATUSES, PROJECT_TEMPLATES
from ..utils import get_choice, get_input
from ..variables import PROJECT_HOME

LOGGER_NAME = os.environ.get("PYTHON_PROJECTUTILS_LOGGER_NAME", DEFAULT_LOGGER_NAME)

logger = logging.getLogger(LOGGER_NAME)

# Constants

# Functions


def factory(path):
    """Load a project from an INI file."""
    name = os.path.basename(path)

    _path = os.path.join(path, "meta.ini")
    if not os.path.exists(_path):
        logger.warning("INI file does not exist for project: %s" % _path)

        return Project(name, path)

    ini = ConfigParser()
    ini.read(_path)

    try:
        kwargs = dict()
        for key, value in ini.items("project"):
            kwargs[key] = value
    except NoSectionError:
        logger.warning("The %s project does not have a project section in the meta.ini file." % name)
        return Project(name, path)

    if "name" in kwargs:
        name = kwargs.pop("name")

    project = Project(name, path, **kwargs)

    for section in ini.sections():
        if section == "project":
            continue

        kwargs = dict()
        for key, value in ini.items(section):
            kwargs[key] = value

        _section = Section(section, **kwargs)
        setattr(project, section, _section)

    return project


def get_projects(path=PROJECT_HOME):
    """Get a list of projects.

    :param path: The path to search.
    :type path: str

    :rtype: list[Project]

    .. note::
        Project instances are *not* loaded. See ``Project.load()``.

    """
    if path is None:
        logger.warning("A path or PROJECT_HOME is required.")
        return None

    # Get the entries from the projects directory. This will raise an OSError if the path does not exist.
    entries = os.listdir(path)
    projects = list()
    for e in entries:
        if e[0] == ".":
            continue

        project = factory(os.path.join(path, e))
        projects.append(project)

    return projects

# Classes


class Input(object):

    def __init__(self):
        self.context = dict()

    def __getattr__(self, item):
        return self.context.get(item)

    def prompt_author(self):
        kwargs = dict()

        kwargs['name'] = get_input("Author Name", default="Unknown")
        kwargs['email'] = get_input("Author Email", default="unknown@example.com")

        author = Section("author", **kwargs)

        self.context['author'] = author

    def prompt_business(self):
        kwargs = dict()

        kwargs['name'] = get_input("Business Name", default="Unknown")
        kwargs['code'] = get_input("Business Code", default="UNK")
        business = Section("business", **kwargs)

        self.context['business'] = business

    def prompt_cla(self):
        choice = get_choice("Include SLA?")
        if choice == "no":
            return None

        kwargs = dict()
        kwargs['email'] = get_input("Legal Email")

        self.context['cla'] = Section("cla", **kwargs)

    def prompt_copyright(self, author_name=None):
        kwargs = dict()

        kwargs['name'] = get_input("Copyright Name", default=author_name)
        kwargs['year'] = get_input("Copyright Year", default=datetime.now().year)

        copyrighting = Section("copyright", **kwargs)

        self.context['copyright'] = copyrighting

    def prompt_license(self):
        kwargs = dict()

        choice = get_choice("License", choices=LICENSE_CHOICES, default="Proprietary")
        # if choice == "Creative Commons":
        #     kwargs['adaptations'] = get_choice("Allow Adaptations?", choices=["Yes", "No", "Share-Alike"])
        #     kwargs['commercial'] = get_choice("Allow Commercial Use?")

        kwargs['name'] = choice
        kwargs['code'] = choice.lower()
        kwargs['url'] = LICENSES[choice]

        licensing = Section("license", **kwargs)

        # TODO: Incorporate CLAs into project init.
        # See https://en.wikipedia.org/wiki/Contributor_License_Agreement
        # See https://www.djangoproject.com/foundation/cla/faq/
        # See https://www.exodusemulator.com/contribute/submit-cla
        # See https://www.clahub.com
        # See http://www.harmonyagreements.org/guide.html
        # See https://www.contributor-covenant.org
        # See also https://github.com/kentcdodds/all-contributors

        self.context['license'] = licensing

    def prompt_project(self, name, path):
        kwargs = dict()

        kwargs['title'] = get_input("Title", default=name.replace("-", " "))
        kwargs['description'] = get_input("Description", default="TODO: Provide a description of the project.")
        # project.category = get_choice("Category", choices=PROJECT_CATEGORY_CHOICES, default="Unclassified")
        kwargs['category'] = get_input("Category", default="unspecified")
        # project.type = get_choice("Type", choices=PROJECT_TYPE_CHOICES, default="Unclassified")
        kwargs['type'] = get_input("Type", default="unspecified")

        kwargs['url'] = get_input("URL", default="http://example.com")

        # cont = "Yes"
        # genres = list()
        # while cont == "Yes":
        #     genre = get_choice("Genre", choices=PROJECT_GENRE_CHOICES, default="Unclassifiable")
        #     genres.append(genre)
        #
        #     cont = get_choice("Add Another Genre?", default="No")
        #
        # project.genres = genres

        kwargs['stage'] = get_choice("Stage", choices=PROJECT_STAGES, default="development")
        kwargs['status'] = get_choice("Status", choices=PROJECT_STATUSES, default="unknown")

        project = Project(name, path, **kwargs)

        self.context['project'] = project
        return project

    def prompt_sla(self):
        choice = get_choice("Include SLA?")
        if choice == "no":
            return None

        kwargs = dict()

        kwargs['company_name'] = get_input("Company Name", default=self.business.name)

        # amount display, amount
        credit_amounts = {
            'one': 1,
            'two': 2,
            'three': 3,
            'four': 4,
            'five': 5,
            'six': 6,
            'seven': 7,
            'eight': 8,
            'nine': 9,
            'ten': 10,
        }
        choice = get_choice("Credit Amount (Percentage)", choices=list(credit_amounts.keys()), default="three")
        kwargs['credit_amount'] = credit_amounts[choice]
        kwargs['credit_amount_display'] = choice

        kwargs['maintenance_start_day'] = get_input("Maintenance Starts On (Day)", default="Saturday")
        kwargs['maintenance_start_hour'] = get_input("Maintenance Starts at (Hour)", default="11am")
        kwargs['maintenance_end_day'] = get_input("Maintenance Ends at (Day)", default="Sunday")
        kwargs['maintenance_end_hour'] = get_input("Maintenance Ends at (Hour)", default="11am")
        kwargs['maintenance_timezone'] = get_input("Maintenance Timezone", default="EST")

        self.context['sla'] = Section("sla", **kwargs)


class Section(object):

    def __init__(self, section_name, **kwargs):
        self._section_name = section_name
        self._attributes = kwargs

    def __getattr__(self, item):
        return self._attributes.get(item)

    def __repr__(self):
        return "<%s %s>" % (self.__class__.__name__, self._section_name)


class Project(Section):

    def __init__(self, name, path, **kwargs):
        self.category = kwargs.pop("category", "unspecified")
        self.description = kwargs.pop("description", None)
        self.name = name
        self.path = path
        self.stage = kwargs.pop("stage", "planning")
        self.tags = list()
        self.title = kwargs.pop("title", name)
        self.type = kwargs.pop("type", "unspecified")

        self._status = kwargs.pop("status", "unknown")

        tags = kwargs.pop("tags", None)
        if tags is not None:
            tags = tags.split(",")
            for t in tags:
                self.tags.append(t.strip())

        super().__init__(name, **kwargs)

    # noinspection PyMethodMayBeStatic
    def get_templates(self):
        """Get the templates used to initialize the project.

        :rtype: dict

        """
        # TODO: Allow the user to specify the path and names of additional project templates.
        return PROJECT_TEMPLATES.copy()

    def has_file(self, name):
        path = os.path.join(self.path, name)
        return os.path.exists(path)

    def read_file(self, name):
        if not self.has_file(name):
            return ""

        path = os.path.join(self.path, name)
        return read_file(path)

    @property
    def status(self):
        if self.has_file(".status"):
            return self.read_file(".status")

        return self._status

    def to_html(self):
        return markdown(self.to_markdown())

    def to_markdown(self):
        a = list()
        a.append("# %s" % self.title)
        a.append("")

        if self.description is not None:
            a.append(self.description)
            a.append("")

        a.append("- Path: %s" % self.path)
        a.append("- Stage: %s" % self.stage)
        a.append("- Status: %s" % self.status)
        a.append("- Category: %s" % self.category)
        a.append("- Type: %s" % self.type)
        a.append("- Version: %s" % self.version)
        a.append("")

        return "\n".join(a)

    def to_rst(self):
        a = list()
        a.append("*" * len(self.title))
        a.append("%s" % self.title)
        a.append("*" * len(self.title))
        a.append("")

        if self.description is not None:
            a.append(self.description)
            a.append("")

        a.append("- Path: %s" % self.path)
        a.append("- Stage: %s" % self.stage)
        a.append("- Status: %s" % self.status)
        a.append("- Category: %s" % self.category)
        a.append("- Type: %s" % self.type)
        a.append("- Version: %s" % self.version)
        a.append("")

        return "\n".join(a)

    def to_text(self):
        a = list()
        a.append(self.title)
        a.append("-" * len(self.title))
        a.append("")

        if self.description is not None:
            a.append(self.description)
            a.append("")

        a.append("Path: %s" % self.path)
        a.append("Stage: %s" % self.stage)
        a.append("Status: %s" % self.status)
        a.append("Category: %s" % self.category)
        a.append("Type: %s" % self.type)
        a.append("Version: %s" % self.version)
        a.append("")

        return "\n".join(a)

    @property
    def version(self):
        if self.has_file("VERSION.txt"):
            return self.read_file("VERSION.txt")

        return "unknown"

    def write_file(self, name, content="", overwrite=False):
        if self.has_file(name) and not overwrite:
            return False

        path = os.path.join(self.path, name)
        write_file(path, content)
