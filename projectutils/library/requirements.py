"""
Utilities for working with project requirements.

.. note::
    These utilities is based on the `ReSrc`_ specification for requirements.

.. _ReSrc: https://resrc.org

Example:

.. code-block:: ini

    [package-name]
    command = The command line, if given, overrides all other options.
    comment = A comment regarding the package.
    docs = The URL for package documentation.
    env = The environment into which the package is installed: base, control, development, live, staging, testing.
          Defaults to base.
    extra = The extra option (included in brackets) for the package install.
    group = The package group: assets, requirements, symlinks. Defaults to requirements.
    home = The URL for the package home page.
    scm = The URL for the package repo.
    url = The URL used to install the package.
    version = The version specifier for the package. This is added as is to package name.

"""

# Imports

from configparser import ConfigParser
from datetime import datetime
import logging
from myninjas.shell.constants import EXIT_ENVIRONMENT, EXIT_OK
from myninjas.utils import write_file
import os
from setuptools import find_packages
from subprocess import getstatusoutput
from ..constants import LOGGER_NAME as DEFAULT_LOGGER_NAME

LOGGER_NAME = os.environ.get("PYTHON_PROJECTUTILS_LOGGER_NAME", DEFAULT_LOGGER_NAME)

logger = logging.getLogger(LOGGER_NAME)

# Constants

PROJECT_HOME = os.environ.get("PROJECT_HOME", None)

# Functions


def build_symlinks(path, preview=False):
    """Create symlinks as needed.

    :param path: Path to the requirements file.
    :type path: str

    :param preview: Preview only.
    :type preview: bool

    """
    if not PROJECT_HOME:
        logger.warning("You must define the PROJECT_HOME environment variable to build local symlinks.")
        return EXIT_ENVIRONMENT

    heading = "Build Symlinks"
    if preview:
        heading += " (Preview)"

    print(heading)
    print("=" * 120)

    packages = Package.get_packages(path=path)
    for package_name, package in packages.items():
        if package.group != "symlinks":
            continue

        # Find the package to use a a link. This *should* always be the first package in the list.
        repo_path = os.path.join(PROJECT_HOME, package_name)
        packages = find_packages(repo_path)
        # print(packages)

        target_name = packages[0]

        from_path = os.path.join(PROJECT_HOME, package_name, target_name)
        # print(from_path)
        if not os.path.exists(from_path):
            logger.warning("(CLONE) %s does NOT exist. Use: git clone %s" % (package_name, package.url))
            # print("\033[0;31m[CLONE]\033[0m %s does NOT exist. Use: git clone %s" % (package_name, package.url))
            continue

        to_path = os.path.join("source", target_name)
        if os.path.exists(to_path):
            logger.info("(EXISTS) %s already exists. There is nothing to do." % to_path)
            # print("\033[0;34m[EXISTS]\033[0m %s already exists. There is nothing to do." % to_path)
        else:
            logger.info("(NEW) %s does not exist. Creating symlink." % to_path)
            # print("\033[0;32m[NEW]\033[0m %s does not exist. Creating symlink." % to_path)

            print("ln -s %s %s" % (from_path, to_path))

            if not preview:
                os.symlink(from_path, to_path)

    return EXIT_OK


def clone_symlinks(path, preview=False):
    """Clone symlink targets as needed.

    :param path: Path to the requirements file.
    :type path: str

    :param preview: Preview only.
    :type preview: bool

    """
    if not PROJECT_HOME:
        logger.warning("You must define the PROJECT_HOME environment variable to clone symlinks.")
        return EXIT_ENVIRONMENT

    heading = "Clone Dependencies"
    if preview:
        heading += " (Preview)"

    print(heading)
    print("=" * 120)

    packages = Package.get_packages(path=path)
    for package_name, package in packages.items():
        if package.group != "symlinks":
            continue

        repo_path = os.path.join(PROJECT_HOME, package_name)

        if not os.path.exists(repo_path):
            logger.info("(NEW) %s does not exist. Cloning now." % repo_path)
            # print("\033[0;32m[NEW]\033[0m %s does not exist. Cloning now." % repo_path)
            cmd = "(cd %s && git clone %s)" % (PROJECT_HOME, package.url)
            print("   %s" % cmd)

            if not preview:
                getstatusoutput(cmd)
        else:
            logger.info("(EXISTS) %s already exists. There is nothing to do." % repo_path)
            # print("\033[0;34m[EXISTS]\033[0m %s already exists. There is nothing to do." % repo_path)

    return EXIT_OK


def generate_docs(path, preview=False):
    """Generate documentation.

    :param path: Path to the requirements file.
    :type path: str

    :param preview: Preview only.
    :type preview: bool

    """
    heading = "Generate Documentation"
    if preview:
        heading += " (Preview)"

    print(heading)
    print("=" * 120)

    text = list()
    text.append(".. Generated by projectutils requirements command %s" % datetime.now())
    text.append("")
    text.append("********")
    text.append("Packages")
    text.append("********")
    text.append("")

    packages = Package.get_packages(path=path)
    for env in Package.ENVIRONMENTS_ORDER:

        env_count = 0
        for package_name, package in packages.items():
            if package.env == env:
                env_count += 1

        if not env_count >= 1:
            continue

        text.append(env.title())
        text.append("=" * len(env))
        text.append("")

        description = Package.ENVIRONMENTS[env]

        if description:
            text.append(description)
            text.append("")

        for package_name, package in packages.items():
            if package.env != env:
                continue

            text.append(package_name)
            text.append("-" * len(package_name))
            text.append("")

            # if package.description:
            #     text.append("%s Part of the %s group." % (package.description, package.group))
            # else:
            text.append("Part of the %s group." % package.group)
            text.append("")

            if package.comment:
                text.append(".. note::")
                text.append("    %s" % package.comment)
                text.append("")

            if package.home:
                text.append("- `Home <%s>`_" % package.home)

            if package.docs:
                text.append("- `Documentation <%s>`_" % package.docs)

            if package.scm:
                text.append("- `Source Code <%s>`_" % package.scm)

            text.append("")

            if package.group == "assets":
                pass
            else:
                text.append("To install:")
                text.append("")
                text.append(".. code-block:: bash")
                text.append("")

                if package.group == "requirements":
                    text.append("    pip install %s;" % package.to_pip())
                else:
                    text.append("    git clone %s;" % package.url)

                text.append("")

    content = ("\n".join(text))
    print(content)

    if not preview:
        output_path = os.path.join("docs", "source", "packages.rst")
        write_file(output_path, content)

    return EXIT_OK


def generate_pip(path, preview=False):
    """Generate PIP files.

    :param path: Path to the requirements file.
    :type path: str

    :param preview: Preview only.
    :type preview: bool

    """
    heading = "Generate PIP Files"
    if preview:
        heading += " (Preview)"

    print(heading)
    print("=" * 120)

    # Get packages from the global requirements.ini file.
    output_files = {
        'base.pip': list(),
        'control.pip': ["-r development.pip"],
        'development.pip': ["-r base.pip"],
        'live.pip': ["-r base.pip"],
        'staging.pip': ["-r base.pip"],
        'testing.pip': ["-r base.pip"],
    }
    packages = Package.get_packages(path=path)
    for env in Package.ENVIRONMENTS.keys():
        file_name = "%s.pip" % env

        for package_name, package in packages.items():

            if package.group != "requirements":
                continue

            if package.env == env:
                output_files[file_name].append(package.to_pip())

    # Find symlinks so they are not duplicated when searching for app-specific requirements.
    symlinks = list()
    for package_name, package in packages.items():

        if package.group == "symlinks":
            symlinks.append(package.to_pip())

    # Build and write the pip files.
    for file_name, requirements in output_files.items():
        requirements.insert(0, "# Generated by projectutils requirements command %s" % datetime.now())
        requirements_content = "\n".join(requirements)

        base_path = os.path.join(os.path.dirname(path), "requirements")
        if not os.path.exists(base_path):
            os.makedirs(base_path)

        output_path = os.path.join(base_path, file_name)
        print(output_path)
        print("-" * 120)
        print(requirements_content)
        print("")

        if not preview:
            write_file(output_path, requirements_content)

    return EXIT_OK


def generate_pipfile(path, preview=False, python_version="3.7"):
    """Generate a Pipenv file.

    :param path: Path to the requirements file.
    :type path: str

    :param preview: Preview only.
    :type preview: bool

    :param python_version: The required version of Python.
    :type python_version: str

    """

    # noinspection PyShadowingNames
    def _get_pip_entry(package):
        # The default version specifier is an asterisk.
        if package.version:
            if package.extra:
                version = '{version = "%s", extras = ["%s"]}' % (package.version, package.extra)
            else:
                version = '"%s"' % package.version
        elif package.url:
            url = package.url.replace("git+", "")
            version = '{git = "%s"}' % url
        else:
            if package.extra:
                version = '{version = "*", extras = ["%s"]}' % package.extra
            else:
                version = '"*"'

        # The package name must be quoted if it contains a number.
        package_name = package.name
        numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        for n in numbers:
            if str(n) in package.name:
                package_name = '"%s"' % package.name

        # Return the spec.
        return '%s = %s' % (package_name, version)

    heading = "Generate Pipfile"
    if preview:
        heading += " (Preview)"

    print(heading)
    print("=" * 120)

    # Load the packages.
    packages = Package.get_packages(path=path)

    # Start the Pipfile output.
    output = list()
    output.append("[[source]]")
    output.append('url = "https://pypi.org/simple"')
    output.append("verify_ssl = true")
    output.append('name = "pypi"')
    output.append("")

    # The base and live environments constitute the [packages] of the Pipfile. All others go into [dev-packages].
    output.append("[packages]")
    for package in packages.values():
        if package.env not in ("base", "live"):
            continue

        # Ignore assets and symlinks.
        if package.group in ("assets", "symlinks"):
            continue

        output.append(_get_pip_entry(package))

    output.append("")

    # All other packages must go into [dev-packages]
    output.append("[dev-packages]")
    for package in packages.values():
        if package.env in ("base", "live"):
            continue

        # Ignore assets and symlinks.
        if package.group in ("assets", "symlinks"):
            continue

        output.append(_get_pip_entry(package))

    output.append("")

    # Add the requires section.
    output.append("[requires]")
    output.append('python_version = "%s"' % python_version)
    output.append("")

    # Output the file.
    output_path = "Pipfile"
    print(output_path)
    print("-" * 120)
    print("\n".join(output))
    print("")

    if not preview:
        write_file(output_path, "\n".join(output))

    return EXIT_OK


def refresh_symlinks(path, preview=False):
    """Fetch (git) symlink targets.

    :param path: Path to the requirements file.
    :type path: str

    :param preview: Preview only.
    :type preview: bool

    """
    if not PROJECT_HOME:
        logger.warning("You must define the PROJECT_HOME environment variable to refresh local symlinks.")
        return EXIT_ENVIRONMENT

    heading = "Refresh Symlinks"
    if preview:
        heading += " (Preview)"

    print(heading)
    print("=" * 120)

    packages = Package.get_packages(path=path)
    for package_name, package in packages.items():
        if package.group != "symlinks":
            continue

        root_path = os.path.join(PROJECT_HOME, package_name)

        if not os.path.exists(root_path):
            logger.info("(CLONE) %s does NOT exist. Use: git clone %s" % (package_name, package.url))
            # print("\033[0;31m[CLONE]\033[0m %s does NOT exist. Use: git clone %s" % (package_name, package.url))
            # print("-" * 120)
            continue

        command = "(cd %s && git fetch)" % root_path
        logger.info("(EXISTS) %s exists. Running: %s" % (root_path, command))
        # print("\033[0;34m[EXISTS]\033[0m %s exists. Running: %s" % (root_path, command))

        if not preview:
            status, output = getstatusoutput(command)
            if output:
                print(output)
                print("+" * 120)

    return EXIT_OK

# Classes


class Package(object):

    # TODO: Move Package class to resrc library.

    ENVIRONMENTS = {
        'base': "The *Base* environment includes the packages that are used across all other environments.",
        'control': "*Control* is a special environment for the local machine and includes packages for documentation "
                   "and deployment.",
        'development': "The *Development* environment represents unstable and untested code that is usually local to "
                       "the developer.",
        'testing': "The *Testing* environment is for automated testing and continuous integration",
        'staging': "The *Staging* environment may be considered the next Live version of the software.",
        'live': "The *Live* environment represents code that is tested, stable, and ready for users.",
    }

    ENVIRONMENTS_ORDER = (
        "base",
        "development",
        "control",
        "testing",
        "staging",
        "live",
    )

    def __init__(self, name, **kwargs):
        self.command = kwargs.get("command", None)
        self.comment = kwargs.get("comment", None)
        self.docs = kwargs.get("docs", None)
        self.env = kwargs.get("env", "base")
        self.extra = kwargs.get("extra", None)
        self.group = kwargs.get("group", "requirements")
        self.home = kwargs.get("home", None)
        self.name = name
        self.scm = kwargs.get("scm", None)
        self.url = kwargs.get("url", None)
        self.version = kwargs.get("version", None)

    @staticmethod
    def get_packages(path="deploy/requirements.ini"):
        """Get all packages from the requirements.ini file.

        :rtype: dict

        """
        # Load the requirements file.
        config = ConfigParser()
        config.read(path)

        # Collect package objects.
        packages = dict()
        for section in config.sections():

            kwargs = dict()
            for key, value in config.items(section):
                kwargs[key] = value

            package = Package(section, **kwargs)

            packages[package.name] = package

        # Return the packages.
        return packages

    def to_pip(self):
        """Get the package as it would appear in a pip file.

        :rtype: str

        """
        if self.command:
            return self.command
        elif self.url:
            return self.url
        elif self.version:
            return "%s%s" % (self.name, self.version)
        elif self.extra:
            return "%s[%s]" % (self.name, self.extra)
        else:
            return self.name
