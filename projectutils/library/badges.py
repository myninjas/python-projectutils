"""
Generate badges using https://shields.io

"""

# Imports

import logging
import os
from ..constants import LOGGER_NAME as DEFAULT_LOGGER_NAME

LOGGER_NAME = os.environ.get("PYTHON_PROJECTUTILS_LOGGER_NAME", DEFAULT_LOGGER_NAME)

logger = logging.getLogger(LOGGER_NAME)

# Exports

__all__ = (
    "STAGES",
    "STATUSES",
    "get_coverage_badge",
    "get_stage_badge",
    "get_status_badge",
)

# Constants

STAGES = {
    'planning': "lightgray",
    'experimental': "red",
    'development': "blue",
    'alpha': "orange",
    'beta': "yellow",
    'release': "yellowgreen",
    'live': "green",
    'obsolete': "000000",
}

STATUSES = {
    'active': "green",
    'hold': "orange",
    'unknown': "gray",
    'maintenance': "yellowgreen",
    'closed': "red",
}

# Functions


def get_coverage_badge(amount):
    if amount == 100:
        color = "green"
    elif amount >= 75:
        color = "yellowgreen"
    elif amount >= 50:
        color = "yellow"
    elif amount >= 25:
        color = "orange"
    else:
        color = "red"

    return "![](https://img.shields.io/badge/coverage-%s%%25-%s.svg)" % (amount, color)


def get_stage_badge(stage):
    if stage not in STAGES:
        logging.warning("Invalid or unrecognized stage: %s" % stage)
        return None

    color = STAGES[stage]
    return "![](https://img.shields.io/badge/stage-%s-%s.svg)" % (stage, color)


def get_status_badge(status):
    if status not in STATUSES:
        logging.warning("Invalid or unrecognized status: %s" % status)
        return None

    color = STATUSES[status]

    if status == "hold":
        status = "on--hold"

    return "![](https://img.shields.io/badge/status-%s-%s.svg)" % (status, color)


# TODO: Implement additional badge support.
'''
PyPI - Django Version:		https://img.shields.io/pypi/djversions/djangorestframework.svg
PyPI - Python Version:		https://img.shields.io/pypi/pyversions/Django.svg
https://shields.io/#/examples/license
https://shields.io/#/examples/build (optionally replace coverage badge)
https://shields.io/#/examples/dependencies
https://requires.io
https://shields.io/#/examples/funding
https://shields.io/#/examples/issue-tracking
https://img.shields.io/stackexchange/stackoverflow/t/augeas.svg
https://img.shields.io/dependabot/semver/bundler/puma.svg
	https://img.shields.io/npm/types/chalk.svg
PyPI - Format:		https://img.shields.io/pypi/format/Django.svg
PyPI - Implementation:		https://img.shields.io/pypi/implementation/Django.svg
PyPI - Status:		https://img.shields.io/pypi/status/Django.svg
PyPI - Wheel:		https://img.shields.io/pypi/wheel/Django.svg
https://shields.io/#/examples/monitoring (enable support for this in currentstatus.net?)
'''
