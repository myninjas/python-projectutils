# Imports

from git import Repo as GitRepo, InvalidGitRepositoryError
import os

# Classes


class Repo(object):
    """A source code repo."""

    def __init__(self, path, cli="git", vendor="github.com"):
        """Initialize a repo instance.

        :param path: The path to the repo.
        :type: path: str

        :param cli: The command line executable.
        :type cli: str || unicode

        :param vendor: The vendor or host of the repo.
        :type vendor: str || unicode

        """
        self.branch = None
        self.cli = cli
        self.is_dirty = None
        self.is_loaded = None
        self.path = path
        self.vendor = vendor

        # Store errors in a list with level and message.
        self._errors = list()

        # Get the appropriate handle for the repo.
        if cli == "git":
            try:
                self._handle = GitRepo(self.path)
                self.branch = str(self._handle.active_branch)
                self.is_dirty = self._handle.is_dirty(untracked_files=True)
            except InvalidGitRepositoryError as e:
                self._errors.append(str(e))
                self._handle = None
                self.branch = "unknown"
                self.is_dirty = None

    def __str__(self):
        return os.path.basename(self.path)
