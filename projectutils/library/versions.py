"""
This utility is based upon `Semantic Versioning`_. By default it operates on ``VERSION.txt`` and
``source/main/version.py``.

.. _Semantic Versioning: http://semver.org

Use ``--version-txt-path`` and ``--version-py-path`` to set the files.

If you do not wish to create a ``version.py`` file, use ``--version-py-path=none``.

Calling the command with no arguments will simply print the current version.

"""
# Imports

import logging
from myninjas.shell.constants import EXIT_IO, EXIT_OK
import os
import semver
from ..constants import LOGGER_NAME as DEFAULT_LOGGER_NAME

LOGGER_NAME = os.environ.get("PYTHON_PROJECTUTILS_LOGGER_NAME", DEFAULT_LOGGER_NAME)

logger = logging.getLogger(LOGGER_NAME)

# Functions


def bumpversion(args):
    """Update project version information."""

    # Load the current version.
    try:
        with open(args.version_txt_path, "r") as f:
            current_version = f.read()
            current_version = current_version.strip()
            f.close()
    except IOError:
        current_version = "0.1.0-d"

    # Just display the current version if no changes are requested.
    actions = [args.build_name or False, args.major_version, args.minor_version, args.patch_level, args.status or False]
    count = 0
    for a in actions:
        if a:
            count += 1

    if count == 0:
        logger.info("Current Version: %s" % current_version)
        return EXIT_OK

    # Get the version instance.
    version = Version(current_version)

    # Use args to update the version.
    new_version = version.bump(
        major=args.major_version,
        minor=args.minor_version,
        patch=args.patch_level,
        status=args.status,
        build=args.build_name
    )

    # Set the default exist code.
    exit_code = 0

    # Preview and write out the new version.
    if args.preview_enabled:
        print("%-30s %-30s" % ("Bump", "To"))
        print("%-30s %-30s" % (current_version, new_version))
    else:
        try:
            with open(args.version_txt_path, "w") as f:
                f.write(new_version)
                f.close()
        except IOError as e:
            logger.warning("Failed to write %s: %s" % (args.version_txt_path, e))
            exit_code = EXIT_IO

        if args.version_py_path != "none":
            info = semver.parse_version_info(new_version)

            try:
                with open(args.version_py_path, "w") as f:
                    f.write("# created by projectutils version command\n")

                    if info.build:
                        f.write('build = "%s"\n' % info.build)
                    else:
                        f.write('build = None\n')

                    f.write('major = %s\n' % info.major)
                    f.write('minor = %s\n' % info.minor)
                    f.write('patch = %s\n' % info.patch)

                    if info.prerelease:
                        f.write('status = "%s"\n' % info.prerelease)
                    else:
                        f.write('status = None\n')

                    f.write('version = "%s"\n' % new_version)

                    f.close()
            except IOError as e:
                logger.warning("Failed to write %s: %s" % (args.version_py_path, e))
                exit_code = EXIT_IO

    return exit_code

# Classes


class Version(object):
    """Represents a version/release string."""

    def __init__(self, identifier):
        self.identifier = identifier

    def __str__(self):
        return self.identifier

    def bump(self, major=False, minor=False, patch=False, status=None, build=None):
        # Get the current version as an instance.
        current_info = semver.parse_version_info(self.identifier)

        # Get the new version.
        if major:
            new_version = semver.bump_major(self.identifier)
        elif minor:
            new_version = semver.bump_minor(self.identifier)
        elif patch:
            new_version = semver.bump_patch(self.identifier)
        else:
            new_version = self.identifier

        # Update the status.
        if status:
            status = status
        elif current_info.prerelease:
            status = current_info.prerelease
        else:
            status = None

        if status:
            info = semver.parse_version_info(new_version)

            new_version = "%s.%s.%s-%s" % (
                info.major,
                info.minor,
                info.patch,
                status
            )

        # Update the build.
        if build:
            info = semver.parse_version_info(new_version)

            new_version = "%s.%s.%s-%s+%s" % (
                info.major,
                info.minor,
                info.patch,
                info.prerelease,
                build
            )

        # Return the new version.
        new_version = new_version.strip()
        return new_version
