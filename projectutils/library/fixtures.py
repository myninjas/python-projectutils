# Imports

from configparser import ConfigParser
import logging
from myninjas.shell.constants import EXIT_INPUT, EXIT_OK, EXIT_UNKNOWN
import os
from subprocess import getstatusoutput
from ..constants import LOGGER_NAME as DEFAULT_LOGGER_NAME

LOGGER_NAME = os.environ.get("PYTHON_PROJECTUTILS_LOGGER_NAME", DEFAULT_LOGGER_NAME)

logger = logging.getLogger(LOGGER_NAME)

# Functions


def dumpdata(args):
    """Dump data using a fixtures.ini file."""

    # Make sure the file exists.
    path = args.path
    if not os.path.exists(path):
        logger.warning("fixtures.ini file does not exist: %s" % path)
        return EXIT_INPUT

    # Load the file.
    ini = ConfigParser()
    ini.read(path)

    # Generate the commands.
    for model in ini.sections():
        kwargs = dict()

        if args.full_preview_enabled:
            kwargs['preview'] = True

        if args.settings:
            kwargs['settings'] = args.settings

        for key, value in ini.items(model):
            kwargs[key] = value

        fixture = Fixture(model, "dumpdata", **kwargs)
        if args.app_labels and fixture.app_label not in args.app_labels:
            logger.info("(SKIPPED) %s" % fixture.model)

            continue

        if args.model_name and fixture.model_name.lower() != args.model_name.lower():
            logger.info("(SKIPPED) %s" % fixture.model)

            continue

        if args.groups and fixture.group not in args.groups:
            logger.info("(SKIPPED) %s" % fixture.model)

            continue

        if fixture.is_readonly:
            logger.info("(READONLY) %s (dumpdata skipped)" % fixture.model)
            continue

        if args.preview_enabled or args.full_preview_enabled:
            logger.info("(PREVIEW) %s" % fixture.preview())

            if args.full_preview_enabled:
                fixture.run()
                print(fixture.output)
        else:
            result = fixture.run()
            if result:
                logger.info("(OK) %s" % fixture.model)
            else:
                logger.info("(FAILED) %s %s" % (fixture.model, fixture.output))
                return EXIT_UNKNOWN

    return EXIT_OK


def loaddata(args):
    """Load data using a fixtures.ini file."""

    path = args.path
    if not os.path.exists(path):
        logger.warning("fixtures.ini file does not exist: %s" % path)
        return EXIT_INPUT

    # Load the file.
    ini = ConfigParser()
    ini.read(path)

    # Generate the commands.
    for model in ini.sections():
        kwargs = dict()

        if args.settings:
            kwargs['settings'] = args.settings

        for key, value in ini.items(model):
            kwargs[key] = value

        fixture = Fixture(model, "loaddata", **kwargs)

        if args.app_labels and fixture.app_label not in args.app_labels:
            if args.preview_enabled:
                logger.info("(SKIPPED) %s" % fixture.model)

            continue

        if args.model_name and fixture.model_name.lower() != args.model_name.lower():
            if args.preview_enabled:
                logger.info("(SKIPPED) %s" % fixture.model)

            continue

        if args.groups and fixture.group not in args.groups:
            if args.preview_enabled:
                logger.info("[SKIPPED] %s" % fixture.model)

            continue

        if args.preview_enabled:
            logger.info("(PREVIEW) %s" % fixture.preview())
        else:
            result = fixture.run()
            if result:
                logger.info("(OK) %s %s" % (fixture.model, fixture.output))
            else:
                logger.warning("(FAILED) %s %s" % (fixture.model, fixture.output))
                return EXIT_UNKNOWN

    return EXIT_OK


# Classes


class Fixture(object):

    def __init__(self, model, operation, **kwargs):
        self.app_label, self.model_name = model.split(".")
        self.database = kwargs.pop("database", None)
        self.group = kwargs.pop("group", "defaults")
        self.is_readonly = kwargs.pop("readonly", False)
        self.model = model
        self.operation = operation
        self.output = None
        self.settings = kwargs.pop("settings", None)
        self.status = None
        self._preview = kwargs.pop("preview", False)

        default_path = os.path.join(self.app_label, "fixtures", self.model_name.lower())
        self.path = kwargs.pop("path", default_path)

    def __repr__(self):
        return "<%s %s.%s>" % (self.__class__.__name__, self.app_label, self.model_name)

    def get_command(self):
        if self.operation == "dumpdata":
            return self._get_dumpdata_command()
        elif self.operation == "loaddata":
            return self._get_loaddata_command()
        else:
            raise ValueError("Invalid fixture operation: %s" % self.operation)

    def preview(self):
        return self.get_command()

    def run(self):
        command = self.get_command()
        status, output = getstatusoutput(command)

        self.output = output
        self.status = status

        if status > EXIT_OK:
            return False

        return True

    def _get_dumpdata_command(self):
        a = list()

        a.append("(cd source && ./manage.py dumpdata")

        if self.settings is not None:
            a.append("--settings=%s" % self.settings)

        if self.database is not None:
            a.append("--database=%s" % self.database)

        # args.full_preview_enabled = True
        if self._preview:
            a.append("--indent=4 %s)" % self.model)
        else:
            a.append("--indent=4 %s > %s)" % (self.model, self.path))

        return " ".join(a)

    def _get_loaddata_command(self):
        a = list()
        a.append("(cd source && ./manage.py loaddata")

        if self.settings is not None:
            a.append("--settings=%s" % self.settings)

        if self.database is not None:
            a.append("--database=%s" % self.database)

        a.append("%s)" % self.path)

        return " ".join(a)
