# Imports

import logging
from myninjas.shell import Command
from myninjas.shell.constants import EXIT_INPUT, EXIT_OK
from myninjas.utils import parse_jinja_template, truncate
import os
from ..constants import LOGGER_NAME as DEFAULT_LOGGER_NAME, TEMPLATE_PATH
from ..library.badges import get_coverage_badge, get_stage_badge, get_status_badge
from ..library.fixtures import dumpdata, loaddata
from ..library.passwords import RandomPassword
from ..library.projects import factory as project_factory, get_projects, Input as ProjectInput
from ..library.requirements import build_symlinks, clone_symlinks, refresh_symlinks, generate_docs, generate_pip, \
    generate_pipfile
from ..library.repos import Repo
from ..library.versions import bumpversion
from ..utils import Table

LOGGER_NAME = os.environ.get("PYTHON_PROJECTUTILS_LOGGER_NAME", DEFAULT_LOGGER_NAME)

logger = logging.getLogger(LOGGER_NAME)

# Commands


def badge_command(args):
    a = list()

    if args.status:
        a.append(get_status_badge(args.status))

    if args.stage:
        a.append(get_stage_badge(args.stage))

    if args.coverage:
        if args.coverage == "?":
            path = os.path.join(os.getcwd(), ".coverage")
            if os.path.exists(path):
                command = Command("coverage report | tail -1")
                if command.run():
                    amount = command.output.split(" ")[-1].replace("'", "").replace("%", "").strip()
                    a.append(get_coverage_badge(int(amount)))
                else:
                    logger.warning("Failed to run coverage report.")
            else:
                logger.warning("No coverage file exists.")
        else:
            a.append(get_coverage_badge(int(args.coverage)))

    print("\n".join(a))

    return EXIT_OK


def dumpdata_command(args):
    return dumpdata(args)


def info_command(args):
    if args.project_name:
        project_name = args.project_name
    else:
        project_name = os.path.basename(os.getcwd())

    path = os.path.join(args.project_path, project_name)
    if not os.path.exists(path):
        logger.warning("The %s project does not exist at: %s" % (project_name, path))
        return EXIT_INPUT

    project = project_factory(path)

    if args.output_format == "html":
        print(project.to_html())
    elif args.output_format == "md":
        print(project.to_markdown())
    elif args.output_format == "rst":
        print(project.to_rst())
    else:
        print(project.to_text())

    return EXIT_OK


def init_command(args):
    logger.debug("Project templates are located at: %s" % TEMPLATE_PATH)

    if args.project_name:
        project_name = args.project_name
    else:
        project_name = os.path.basename(os.getcwd())

    logger.debug("Project name: %s" % project_name)

    path = os.path.join(args.project_path, project_name)
    if not os.path.exists(path):
        logger.warning("The %s project does not exist at: %s" % (project_name, path))
        return EXIT_INPUT

    logger.debug("Project path: %s" % path)

    # Collect input.
    form = ProjectInput()
    project = form.prompt_project(project_name, path)
    form.prompt_author()
    form.prompt_business()
    form.prompt_copyright(author_name=form.author.name)
    form.prompt_license()
    form.prompt_sla()
    form.prompt_cla()

    # Create templates.
    templates = project.get_templates()

    if not form.cla:
        del templates['cla-corporate.markdown.j2']
        del templates['cla-individual.markdown.j2']

    if not form.sla:
        del templates['sla.markdown.j2']
        del templates['sla.rst.j2']
        del templates['sla.txt.j2']

    for template_name, file_name in templates.items():
        from_path = os.path.join(TEMPLATE_PATH, template_name)
        logger.debug("Load %s template: %s" % (file_name, from_path))

        content = parse_jinja_template(from_path, form.context)
        project.write_file(file_name, content)

    return EXIT_OK


def loaddata_command(args):
    return loaddata(args)


def ls_command(args):
    headings = [
        "Title",
        "Category",
        "Type",
        "Stage",
        "Status",
        "Version",
    ]

    if args.repo_enabled:
        headings.append("Repo")

    if args.output_format == "console":
        formatting = "fancy_grid"
    elif args.output_format == "md":
        formatting = "pipe"
    else:
        formatting = args.output_format

    table = Table(formatting=formatting, headings=headings)

    projects = get_projects()
    for p in projects:

        title = truncate(p.title)

        values = [
            title,
            p.category,
            p.type,
            p.stage,
            p.status,
            p.version,
        ]

        if args.repo_enabled:
            repo = Repo(p.path)
            value = repo.branch
            if repo.is_dirty:
                value += "*"

            values.append(value)

        table.add(values)

    print(table)

    return 0


def password_command(args):
    password_length = 10
    if args.strong:
        password_length = 20

    password = RandomPassword(password_length, use_unambiguous=args.use_unambiguous)

    if args.format == "crypt":
        print(password.to_crypt())
    elif args.format == "htpasswd":
        print(password.to_htpasswd())
    elif args.format == "md5":
        print(password.to_md5())
    else:
        print(password.plain_text)

    return EXIT_OK


def requirements_command(args):
    # Build based on options. Functions may use logging, print output, and return an exit code.
    if args.build_symlinks:
        return build_symlinks(args.requirements_path, preview=args.preview_enabled)
    elif args.clone_symlinks:
        return clone_symlinks(args.requirements_path, preview=args.preview_enabled)
    elif args.refresh_symlinks:
        return refresh_symlinks(args.requirements_path, preview=args.preview_enabled)
    elif args.generate_docs:
        return generate_docs(args.requirements_path, preview=args.preview_enabled)
    elif args.generate_pipfile:
        return generate_pipfile(args.requirements_path, preview=args.preview_enabled, python_version=args.python_version)
    else:
        return generate_pip(args.requirements_path, preview=args.preview_enabled)


def version_command(args):
    return bumpversion(args)
