# Imports

from argparse import ArgumentParser, RawDescriptionHelpFormatter
import logging
from myninjas.shell import EXIT_SUCCESS, EXIT_UNKNOWN
from myninjas.logging import LoggingHelper
import os
import sys
from .subcommands import badge_command, dumpdata_command, info_command, init_command, loaddata_command, ls_command, \
    password_command, requirements_command, version_command
from ..constants import LOGGER_NAME as DEFAULT_LOGGER_NAME
from .initialize import SubCommands

LOGGER_NAME = os.environ.get("PYTHON_PROJECTUTILS_LOGGER_NAME", DEFAULT_LOGGER_NAME)

log = LoggingHelper(colorize=True, name=LOGGER_NAME)
logger = log.setup()

# Commands


def main_command():
    """Work with a project using various subcommands."""

    __author__ = "Shawn Davis <shawn@develmaycare.com>"
    __date__ = "2018-10-29"
    __help__ = """NOTES

The project command is the entry point for various subcommands that make working with a project (and projects) easier.

    """
    __version__ = "0.8.1-d"

    parser = ArgumentParser(description=__doc__, epilog=__help__, formatter_class=RawDescriptionHelpFormatter)

    # Initialize sub-commands.
    subparsers = parser.add_subparsers(
        dest="subcommand",
        help="Commands",
        metavar="badge, dumpdata, info, init, loaddata, ls, password, requirements, version"
    )

    commands = SubCommands(subparsers)
    commands.badge()
    commands.dumpdata()
    commands.info()
    commands.init()
    commands.loaddata()
    commands.ls()
    commands.password()
    commands.requirements()
    commands.version()

    # Access to the version number requires special consideration, especially
    # when using sub parsers. The Python 3.3 behavior is different. See this
    # answer: http://stackoverflow.com/questions/8521612/argparse-optional-subparser-for-version
    parser.add_argument(
        "-v",
        action="version",
        help="Show version number and exit.",
        version=__version__
    )

    parser.add_argument(
        "--version",
        action="version",
        help="Show verbose version information and exit.",
        version="%(prog)s" + " %s %s by %s" % (__version__, __date__, __author__)
    )

    # Parse the given arguments.
    args = parser.parse_args()
    command = args.subcommand

    if args.debug_enabled:
        logger.setLevel(logging.DEBUG)
        logger.debug(str(args))

    # Run the requested command.
    result = 0
    if command == "badge":
        result = badge_command(args)
    elif command in ("dd", "dumpdata"):
        result = dumpdata_command(args)
    elif command == "info":
        result = info_command(args)
    elif command == "init":
        result = init_command(args)
    elif command in ("ld", "loaddata"):
        result = loaddata_command(args)
    elif command in ("list", "listing", "ls"):
        result = ls_command(args)
    elif command in ("pass", "password", "pw"):
        result = password_command(args)
    elif command in ("req", "requirements", "reqs"):
        result = requirements_command(args)
    elif command in ("bumpversion", "ver", "version"):
        version_command(args)
    else:
        logger.warning("Command not implemented: %s" % command)
        result = False

    # Quit.
    if result == 0:
        sys.exit(EXIT_SUCCESS)
    elif result > 0:
        sys.exit(result)
    else:
        sys.exit(EXIT_UNKNOWN)
