# Imports

from configparser import ConfigParser
import logging
import os
from ..constants import LOGGER_NAME as DEFAULT_LOGGER_NAME
from ..library.badges import STAGES, STATUSES
from ..variables import DEPLOY_PATH, PROJECT_HOME
# from ..contexts import Context
# from ..variables import factory as variable_factory, Variable

LOGGER_NAME = os.environ.get("PYTHON_PROJECTUTILS_LOGGER_NAME", DEFAULT_LOGGER_NAME)

logger = logging.getLogger(LOGGER_NAME)

# Constants

# Functions


def initialize_config(path):
    """Get the current user's configuration options.

    :rtype: dict()

    """
    if not os.path.exists(path):
        return dict()

    ini = ConfigParser()
    ini.read(path)


# def initialize_context(args):
#     """Initialize the global context.
#
#     This processes the variables passed from the command line as well as the ``variables.ini`` file if one has been
#     provided.
#
#     :rtype: Context
#
#     """
#     context = Context()
#
#     if args.variables:
#         for spec in args.variables:
#             name, value = spec.split(":")
#
#             name = name.strip()
#             value = value.strip()
#
#             context.add(
#                 name,
#                 comment="%s initialized from command line." % name,
#                 value=value
#             )
#
#     variables_path = os.path.join(args.base_path, "variables.ini")
#     if os.path.exists(variables_path):
#         logger.info("Load data from %s" % variables_path)
#         variables = variable_factory(variables_path)
#
#         for v in variables:
#             context.append(v)
#
#         # for env in args.environments:
#         #     for v in _variables:
#         #         if env in v.environments:
#         #             if scope == SCOPE_ALL:
#         #                 variables.append(v)
#         #             elif scope == v.scope:
#         #                 variables.append(v)
#         #             else:
#         #                 pass
#
#         logger.debug("Globals: %s" % variables)
#
#     return context


# Classes


class SubCommands(object):
    """A utility class which keeps the ``cli`` package clean."""

    def __init__(self, subparsers):
        self.subparsers = subparsers

    def badge(self):
        """Initialize the badge sub-command."""
        sub = self.subparsers.add_parser(
            "badge",
            help="Create project badges."
        )

        sub.add_argument(
            "-c=",
            "--coverage=",
            dest="coverage",
            help="The amount (int) of test coverage."
        )

        sub.add_argument(
            "-g=",
            "--stage=",
            choices=list(STAGES.keys()),
            dest="stage",
            help="Create a stage badge."
        )

        sub.add_argument(
            "-t=",
            "--status=",
            choices=list(STATUSES.keys()),
            dest="status",
            help="Create a status badge."
        )

        self._add_common_options(sub)

    def dumpdata(self):
        """Initialize the dumpdata sub-command."""
        sub = self.subparsers.add_parser(
            "dumpdata",
            aliases=["dd"],
            help="Dump Django fixtures."
        )

        sub.add_argument(
            "-a=",
            "--app=",
            action="append",
            dest="app_labels",
            help="Only include fixtures for the given app."
        )

        sub.add_argument(
            "-g=",
            "--group=",
            action="append",
            dest="groups",
            help="Limit activities to these fixture groups."
        )

        sub.add_argument(
            "-m=",
            "--model=",
            dest="model_name",
            help="Only include fixtures for the given model."
        )

        sub.add_argument(
            "-P=",
            "--path=",
            default="%s/fixtures.ini" % DEPLOY_PATH,
            dest="path",
            help="Path to the fixtures.ini file. Defaults to: %s/fixtures.ini" % DEPLOY_PATH
        )

        sub.add_argument(
            "--print",
            action="store_true",
            dest="full_preview_enabled",
            help="Preview the output of the dumdata commands."
        )

        sub.add_argument(
            "-S=",
            "--settings=",
            dest="settings",
            help="Specify the settings to use for the management command."
        )

        self._add_common_options(sub)

    def info(self):
        """Initialize the info sub-command."""
        sub = self.subparsers.add_parser(
            "info",
            help="Display project info."
        )

        sub.add_argument(
            "project_name",
            help="Specify the project. If the project name is omitted, it defaults to the current working directory.",
            nargs="?"
        )

        sub.add_argument(
            "-f=",
            "--format=",
            choices=["html", "md", "plain", "rst"],
            default="plain",
            dest="output_format",
            help="The format of output."
        )

        sub.add_argument(
            "-P=",
            "--path=",
            default=PROJECT_HOME,
            dest="project_path",
            help="Path to where projects are stored. Default: $PROJECT_HOME "
        )

        self._add_common_options(sub)

    def init(self):
        """Initialize the init sub-command."""
        sub = self.subparsers.add_parser(
            "init",
            help="Initialize a project. This command runs interactively."
        )

        sub.add_argument(
            "project_name",
            help="Specify the project. If the project name is omitted, it defaults to the current working directory.",
            nargs="?"
        )

        sub.add_argument(
            "-P=",
            "--path=",
            default=PROJECT_HOME,
            dest="project_path",
            help="Path to where projects are stored. Default: $PROJECT_HOME "
        )

        self._add_common_options(sub)

    def loaddata(self):
        """Initialize the loaddata sub-command."""
        sub = self.subparsers.add_parser(
            "loaddata",
            aliases=["ld"],
            help="Load Django fixtures."
        )

        sub.add_argument(
            "-a=",
            "--app=",
            action="append",
            dest="app_labels",
            help="Only include fixtures for the given app."
        )

        sub.add_argument(
            "-g=",
            "--group=",
            action="append",
            dest="groups",
            help="Limit activities to these fixture groups."
        )

        sub.add_argument(
            "-m=",
            "--model=",
            dest="model_name",
            help="Only include fixtures for the given model."
        )

        sub.add_argument(
            "-P=",
            "--path=",
            default="%s/fixtures.ini" % DEPLOY_PATH,
            dest="path",
            help="Path to the fixtures.ini file. Defaults to: %s/fixtures.ini" % DEPLOY_PATH
        )

        sub.add_argument(
            "-S=",
            "--settings=",
            dest="settings",
            help="Specify the settings to use for the management command."
        )

        self._add_common_options(sub)

    def ls(self):
        """Initialize the ls sub-command."""
        print("initialize ls")
        sub = self.subparsers.add_parser(
            "ls",
            aliases=["list", "listing"],
            help="List projects."
        )

        sub.add_argument(
            "-f=",
            "--format=",
            choices=["console", "html", "md", "plain", "rst"],
            default="console",
            dest="output_format",
            help="The format of output."
        )

        sub.add_argument(
            "-r",
            "--repo",
            action="store_true",
            dest="repo_enabled",
            help="Include current SCM status. List takes longer to build."
        )

        self._add_common_options(sub)

    def password(self):
        """Initialize the password sub-command."""
        sub = self.subparsers.add_parser(
            "password",
            aliases=["pass", "pw"],
            help="Generate a random password."
        )

        sub.add_argument(
            "--format=",
            choices=["crypt", "md5", "plain", "htpasswd"],
            default="plain",
            dest="format",
            help="Choose the format of the output.",
            nargs="?"
        )

        sub.add_argument(
            "--strong",
            action="store_true",
            help="Make the password stronger."
        )

        sub.add_argument(
            "-U",
            action="store_true",
            dest="use_unambiguous",
            help="Avoid ambiguous characters."
        )

        self._add_common_options(sub)

    def requirements(self):
        """Initialize the requirements sub-command."""
        sub = self.subparsers.add_parser(
            "requirements",
            aliases=["req", "reqs"],
            help="Work with project requirements."
        )

        sub.add_argument(
            "-c",
            "--clone",
            action="store_true",
            dest="clone_symlinks",
            help="Clone the symlinks that do not already exist."
        )

        sub.add_argument(
            "-d",
            "--docs",
            action="store_true",
            dest="generate_docs",
            help="Output packages as RST documentation."
        )

        sub.add_argument(
            "-P=",
            "--path=",
            default="%s/requirements.ini" % DEPLOY_PATH,
            dest="requirements_path",
            help="Path to the requirements.ini file. Default: %s/requirements.ini" % DEPLOY_PATH
        )

        sub.add_argument(
            "--pipfile",
            action="store_true",
            dest="generate_pipfile",
            help="Generate a Pipfile."
        )

        sub.add_argument(
            "--python=",
            default="3.7",
            dest="python_version",
            help="Specify the Python version to use for the Pipfile."
        )

        sub.add_argument(
            "-r",
            "--refresh",
            action="store_true",
            dest="refresh_symlinks",
            help="Refresh local symlinks (run git fetch)."
        )

        sub.add_argument(
            "-s",
            "--symlinks",
            action="store_true",
            dest="build_symlinks",
            help="Build symlinks to local symlinks."
        )

        self._add_common_options(sub)

    def version(self):
        """Initialize the version subcommand."""
        sub = self.subparsers.add_parser(
            "version",
            help="Work with project version."
        )

        sub.add_argument(
            "-b=",
            "--build=",
            dest="build_name",
            help="An optional build name."
        )

        sub.add_argument(
            "-M",
            "--major",
            action="store_true",
            dest="major_version",
            help="Bumping the major version resets the minor and patch level."
        )

        sub.add_argument(
            "-m",
            "--minor",
            action="store_true",
            dest="minor_version",
            help="Bumping the minor version resets the patch level."
        )

        sub.add_argument(
            "-P",
            "--patch",
            action="store_true",
            dest="patch_level",
            help="Bump the patch level."
        )

        sub.add_argument(
            "-s=",
            "--status=",
            dest="status",
            help="Set the status."
        )

        sub.add_argument(
            "--version-py-path=",
            default=os.path.join("source", "main", "version.py"),
            dest="version_py_path",
            help="Path to version.py file. See NOTES. Defaults to source/main/version.py"
        )

        sub.add_argument(
            "--version-txt-path=",
            default="VERSION.txt",
            dest="version_txt_path",
            help="Path to the VERSION.txt file. Defaults to: VERSION.txt"
        )

        self._add_common_options(sub)

    # noinspection PyMethodMayBeStatic
    def _add_common_options(self, subcommand):
        """Add the common switches to a given sub-command instance.

        :param subcommand: The sub-command instance.

        """
        default_config_path = os.path.expanduser(os.path.join("~/.projectutils", "config.ini"))
        subcommand.add_argument(
            "-C=",
            "--config=",
            default=default_config_path,
            dest="config_path",
            help="The path to your projectutils config file. Default: %s" % default_config_path
        )

        subcommand.add_argument(
            "-D",
            "--debug",
            action="store_true",
            dest="debug_enabled",
            help="Enable debug mode. Produces extra output."
        )

        subcommand.add_argument(
            "-p",
            "--preview",
            action="store_true",
            dest="preview_enabled",
            help="Preview the actions to be taken."
        )

        # subcommand.add_argument(
        #     "-e=",
        #     "--env=",
        #     action="append",
        #     dest="environments",
        #     help="The environments for which commands will be prepared or executed. This option may be used multiple "
        #          "times. The base environment is included by default."
        # )
        #
        # subcommand.add_argument(
        #     "-s=",
        #     "--scope=",
        #     choices=["all", "deploy", "provision", "tenant"],
        #     default="deploy",
        #     dest="scope",
        #     help="The scope of commands to prepare or run. Default: %s" % SCOPE_DEPLOY
        # )
        #
        # subcommand.add_argument(
        #     "-V=",
        #     "--variable=",
        #     action="append",
        #     dest="variables",
        #     help='Specify variable overrides in the form of "name:value". Use quotes as needed. This option may be '
        #          'used multiple times.'
        # )
