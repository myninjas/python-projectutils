import os
from .constants import LOGGER_NAME as DEFAULT_LOGGER_NAME

LOGGER_NAME = os.environ.get("PYTHON_PROJECTUTILS_LOGGER_NAME", DEFAULT_LOGGER_NAME)

SKELETON_LOCATIONS = [
    os.path.abspath(os.path.join(os.path.dirname(__file__), "skeletons")),
]

CUSTOM_SKELETON_PATH = os.environ.get("PYTHON_PROJECTUTILS_SKELETON_PATH", None)
if CUSTOM_SKELETON_PATH is not None:
    SKELETON_LOCATIONS.insert(0, CUSTOM_SKELETON_PATH)

TEMPLATE_LOCATIONS = [
    os.path.abspath(os.path.join(os.path.dirname(__file__), "templates")),
]

CUSTOM_TEMPLATE_PATH = os.environ.get("PYTHON_PROJECTUTILS_TEMPLATE_PATH", None)
if CUSTOM_TEMPLATE_PATH is not None:
    TEMPLATE_LOCATIONS.insert(0, CUSTOM_TEMPLATE_PATH)

if os.path.exists(os.path.join("deployable", "deploy")):
    DEPLOY_PATH = os.path.join("deployable", "deploy")
else:
    DEPLOY_PATH = "deploy"

PROJECT_HOME = os.environ.get("PROJECT_HOME", os.path.expanduser("~/Work"))
