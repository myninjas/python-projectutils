# Python Project Utils

![](https://img.shields.io/badge/status-active-green.svg)
![](https://img.shields.io/badge/stage-development-blue.svg)
![](https://img.shields.io/badge/coverage-0%25-red.svg)

A collection of documentation and command line utilities for managing a software project.

## Install

You may install the utilities in a virtual environment specific to a project or as part of your local Python environment to 
make them available to all projects:

```bash
pip install git+https://bitbucket.org/myninjas/python-projectutils/master.tar.gz;
```

## Usage

Most commands must be run from the "project root"; this is the top-level directory of the project where things like 
``VERSION.txt`` or ``setup.py`` are stored.

For Django projects, the project is assumed to live in ``source/`` and project root is the level above source.

### Commands

The ``projectutils`` command is the entry point for using sub-commands, that is, ``projectutils <command_name>``.

- ``badge``: Generate various project badges for use in documentation.
- ``dumpdata``: Dump Django fixtures using a ``deploy/fixtures.ini`` file.
- ``info``: Get info (meta data) on a project.
- ``init``: Initialize a project.
- ``loaddata``: Load Django fixtures using a ``deploy/fixtures.ini`` file.
- ``ls``: List the projects in ``$PROJECT_HOME``.
- ``password``: Generate a password.
- ``requirements``: Works with the ``deploy/requirements.ini`` file to build pip install files and generate 
   documentation.
- ``version``: Automatically increase the project version/release.

### Fixture Files

The ``deploy/fixtures.ini`` is an INI file for more easily managing Django fixtures without the need for manually 
entering commands.

```ini
; comments and blank lines are ignored.
; The most basic usage requires only an app label. The following entry will save/load fixtures at 
; my_app/fixtures/initial.json
[my_app]

; You may also specify a model, which is output using the the lower case name of the model. The entry below would 
; save/load fixtures for MyModel to my_app/fixtures/mymodel.json
[my_app.MyModel]

; You can specify a path to save fixtures.
[my_app.MyModel]
path = local/my_app/fixtures/examples.json

```

### Requirements File

The ``deploy/requirements.ini`` file is an alternative to a pip install file that may be used to generate an install 
file, output documentation, and handle special cases.

```ini
; The package name is in brackets.
[ansible]
comment = Used for deploying the software.
env = control
group = requirements
docs = http://docs.ansible.com/ansible/latest/index.html

; Use the url key to specify packages to be installed from git.
[django-htmgel]
group = requirements
url = git+https://github.com/develmaycare/django-htmgel.git
```

- The install command is automatically generated unless the ``command`` key is provided. Note that this command must be 
  able to run from within a pip install file.
- The ``comment`` key may be used to add additional information to documentation, such as how the package is used or why
  it was selected, or any another notes.
- ``env`` should be one of ``development``, ``control``, ``testing``, ``staging``, or ``live``. The ``base`` environment 
  is the default and need not be specified.
- The ``group`` is one of ``assets``, ``requirements``, or ``symlinks``. The default is ``requirements`` and is 
  always used to generate a pip install file. ``symlinks`` is used to create symlinks to packages on the local
  machine, while ``assets`` is only used when creating documentation.
- Important URLs are provided by ``docs``, ``home``, and ``scm``.
